var net = require('net');
 
if(process.argv.length != 4){
	console.log("Usage: node %s <host> <port>", process.argv[1]);
	process.exit(1);	
}

var host=process.argv[2];
var port=process.argv[3];

if (host.length > 253 || port.length > 5) {
	console.log("Invalid host or port. Try again!\nUsage: node %s <port>", process.argv[1]);
	process.exit(1);	
}

var client = new net.Socket();
console.log("Simple chatclient.js developed by Jared Aronson, SecAD");
console.log("Connecting to: %s:%s", host, port);

client.connect(port,host, connected);

function connected() {
	console.log("Connected to: %s:%s", client.remoteAddress, client.remotePort);
	loginsync();
}

function talk() {
	const keyboard = require('readline').createInterface({
		input: process.stdin,
		output: process.stdout
	});
	keyboard.on('line', function(input) {
		console.log("You typed: " + input);
		client.write(input);
	});

	client.on("data", function(data) {
		console.log("Received data: " + data);
	});

	client.on("error", function(err) {
		console.log("Error");
		process.exit(2);
	});

	client.on("close", function(data) {
		console.log("Connection has been disconnected");
		process.exit(3);
	});
}

var readlineSync = require('readline-sync')
var username;
var password;
function loginsync() {
	// Wait for user's response.
	username = readlineSync.question('Username:');
	if (!inputValidated(username)) {
		console.log("Username must have at least 5 characters. Please try again!");
		loginsync();
		return;
	}
	// Handle the secret text (e.g. password).
	password = readlineSync.question('Password:', {
		hideEchoBack: true // The typed text on screen is hidden by `*` (default).
	});
	if (!inputValidated(password)) {
		console.log("Password must have at least 5 characters. Please try again!");
		loginsync();
		return;
	}
	var login = "{\"Username\":\"" + username + "\", \"Password\":\"" + password + "\"}";
	client.write(login);
	talk();
}

function inputValidated(input) {
	return input.length >= 5;
}
