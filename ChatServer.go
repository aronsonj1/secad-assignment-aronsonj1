package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net"
	"os"
)

const BUFFERSIZE int = 1024
var allLoggedIn_conns = make(map[net.Conn]string)

func main() {
	if len(os.Args) != 2 {
		fmt.Printf("Usage: %s <port>\n", os.Args[0])
		os.Exit(0)
	}
	port := os.Args[1]
	if len(port) > 5 {
		fmt.Print("Invalid port value. Try again!\n")
		os.Exit(1)
	}
	server, err := net.Listen("tcp", ":" + port)
	if err != nil {
		fmt.Printf("Cannot listen on port %s!\n", port)
		os.Exit(2)
	}
	fmt.Print("ChatServer in GoLang developed by Jared Aronson, SecAD\n")
	fmt.Printf("ChatServer is listening on port '%s' ...\n", port)
	newclient := make(chan net.Conn)
	go func() {
		for {
			client_conn, _ := server.Accept()
			newclient <- client_conn
		}
	}()
	for {
		select {
			case client_conn := <-newclient:
				go login(client_conn)
		}
	}
}

func login(client_conn net.Conn) {
	/* parse a formatted data (e.g., username=abc;password=123) */
	var buffer [BUFFERSIZE]byte
	lostclient := make(chan net.Conn)
	byte_received, read_err := client_conn.Read(buffer[0:])
	if read_err != nil {
		fmt.Println("Error in receiving...")
		lostclient <- client_conn
		return
	}
	
	check, username, _ := checklogin(buffer[0:byte_received])

	if check {
		allLoggedIn_conns[client_conn] = username
		client_goroutine(client_conn)
	} else {
		failmessage := []byte("The credentials you provided cannot be determined to be authentic\n")
		sendto(client_conn, failmessage)
	}
}

func checklogin(data []byte) (bool, string, string) {
	// expecting format of {"username":"..","password":".."}
	type Account struct {
		Username string
		Password string
	}
	var account Account
	err := json.Unmarshal(data, &account)
	if err != nil || account.Username == "" || account.Password == "" {
		fmt.Printf("JSON parsing error: %s\n", err)
		return false, "", `[BAD LOGIN Expected: {"Username":"..","Password":".."}`
	}

	if checkaccount(account.Username, account.Password) {
		return true, account.Username, "logged"
	}

	return false, "", "Invalid username or password\n"
}

func checkaccount(username string, password string) bool {
	file, err := os.Open("logins.json")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	type Account struct {
		Username string
		Password string
	}

	dec := json.NewDecoder(bufio.NewReader(file))

	for {
		var account Account
		if err := dec.Decode(&account); err == io.EOF {
			break
		} else if err != nil {
			log.Fatal(err)
		}
		if account.Username == username && account.Password == password {
			return true;
		}
	}

	return false;
}

func client_goroutine(client_conn net.Conn) {
	welcomemessage := fmt.Sprintf("A new client '%s' connected!\n# of connected clients: %d\n", client_conn.RemoteAddr().String(), len(allLoggedIn_conns))
	fmt.Println(welcomemessage)
	sendtoAll([]byte (welcomemessage))
	userlist := []byte(userList())
	fmt.Println(userlist)
	sendtoAll(userlist)
	helpmessage := []byte("List of server commands:\n.exit: Leave the server\n.userlist: Print a list of all active users\n.public: Send future messages to all active users\n.private username: Send future messages to username\n")
	sendto(client_conn, helpmessage)
	var buffer [BUFFERSIZE]byte
	lostclient := make(chan net.Conn)
	var private bool
	private = false
	var destination string
	go func() {
		for {
			byte_received, read_err := client_conn.Read(buffer[0:])
			if read_err != nil {
				fmt.Println("Error in receiving...")
				lostclient <- client_conn
				return
			}
			if string(buffer[0:5]) == ".exit" {
				byemessage := fmt.Sprintf("A client is disconnected " + client_conn.RemoteAddr().String() + "\n")
				delete(allLoggedIn_conns, client_conn)
				go sendtoAll([]byte(byemessage))
			} else if string(buffer[0:9]) == ".userlist" {
				userlist := []byte(userList())
				sendto(client_conn, userlist)
			} else if string(buffer[0:7]) == ".public" {
				publicmessage := []byte("Sending messages to everybody.\n")
				sendto(client_conn, publicmessage)
				private = false
			} else if string(buffer[0:9]) == ".private " && byte_received >= 14 {
				privatemessage := []byte(fmt.Sprintf("Sending private messages to " + string(buffer[9:byte_received]) + "\n"))
				sendto(client_conn, privatemessage)
				destination = string(buffer[9:byte_received])
				private = true
			} else if string(buffer[0:1]) == "." {
				helpmessage := []byte("List of server commands:\n.exit: Leave the server\n.userlist: Print a list of all active users\n.public: Send future messages to all active users\n.private username: Send future messages to username\n")
				sendto(client_conn, helpmessage)
			} else {
				if private {
					go sendtoPrivate(destination, append([]byte(fmt.Sprintf("Private message from %s:", allLoggedIn_conns[client_conn])), buffer[0:byte_received]...))
				} else {
					go sendtoAll(append([]byte(fmt.Sprintf("Public message from %s:", allLoggedIn_conns[client_conn])), buffer[0:byte_received]...))
				}
			}
		}
	}()
	for {
		select {
			case client_conn := <-lostclient:
				byemessage := fmt.Sprintf("A client is disconnected " + client_conn.RemoteAddr().String() + "\n")
				delete(allLoggedIn_conns, client_conn)
				go sendtoAll([]byte(byemessage))
		}
	}
}

func userList() string {
	var userlist string
	for loggedIn_conn, _ := range allLoggedIn_conns {
		if userlist != "" {
			userlist += ", "
		}
		userlist += allLoggedIn_conns[loggedIn_conn]
	}
	return userlist
}

func sendtoAll(data []byte) {
	for loggedIn_conn, _ := range allLoggedIn_conns {
		_, write_err := loggedIn_conn.Write(data)
		if write_err != nil {
			fmt.Println("Error in sending...")
			continue
		}
	}
	fmt.Printf("Send data: %s to all clients!\n", data)
}

func sendtoPrivate(username string, data []byte) {
	for loggedIn_conn, _ := range allLoggedIn_conns {
		if allLoggedIn_conns[loggedIn_conn] == username {
			sendto(loggedIn_conn, data)
		}
	}
}

func sendto(client_conn net.Conn, data []byte) {
	_, write_err := client_conn.Write(data)
	if write_err != nil {
		fmt.Println("Error in sending...")
		return
	}
	fmt.Printf("Send data: %s to %s!\n", data, allLoggedIn_conns[client_conn])
}
